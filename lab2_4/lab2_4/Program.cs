﻿class Program
{
    static void Main(string[] args)
    {
        if (args.Length == 0)
        {
            Console.WriteLine("Usage: dotnet run -- <directory1> <directory2> ...");
            Environment.Exit(1);
        }

        foreach (string directoryPath in args)
        {
            CalculateSubdirectorySizes(directoryPath);
        }

        Environment.Exit(0);
    }

    static void CalculateSubdirectorySizes(string directoryPath)
    {
        try
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);

            if (!directoryInfo.Exists)
            {
                Console.WriteLine($"Directory '{directoryPath}' does not exist.");
                return;
            }

            Console.WriteLine($"Calculating sizes of subdirectories in: {directoryPath}");
            Console.WriteLine("-----------------------------------------");

            DirectoryInfo[] subDirectories = directoryInfo.GetDirectories();
            
            foreach (DirectoryInfo subDirectory in subDirectories)
            {
                long subdirectorySize = CalculateDirectorySize(subDirectory);
                
                Console.WriteLine($"{subDirectory.Name}: {FormatSize(subdirectorySize)}");
            }

            Console.WriteLine();
        }
        catch (UnauthorizedAccessException)
        {
            Console.WriteLine($"Access to directory '{directoryPath}' is denied.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while calculating directory sizes: {ex.Message}");
        }
    }

    static long CalculateDirectorySize(DirectoryInfo directoryInfo)
    {
        long totalSize = 0;

        try
        {
            FileInfo[] files = directoryInfo.GetFiles();

            foreach (FileInfo file in files)
            {
                totalSize += file.Length;
            }

            DirectoryInfo[] subDirectories = directoryInfo.GetDirectories();

            foreach (DirectoryInfo subDirectory in subDirectories)
            {
                totalSize += CalculateDirectorySize(subDirectory);
            }
        }
        catch (UnauthorizedAccessException)
        {
            Console.WriteLine($"Access to directory '{directoryInfo.FullName}' is denied.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while calculating directory sizes: {ex.Message}");
        }

        return totalSize;
    }

    static string FormatSize(long size)
    {
        string[] suffixes = { "B", "KB", "MB", "GB", "TB" };
        int suffixIndex = 0;
        decimal sizeBytes = size;

        while (sizeBytes >= 1024 && suffixIndex < suffixes.Length - 1)
        {
            sizeBytes /= 1024;
            suffixIndex++;
        }

        return $"{sizeBytes:0.##} {suffixes[suffixIndex]}";
    }
}